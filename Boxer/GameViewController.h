//
//  GameViewController.h
//  Boxer
//

//  Copyright (c) 2016 Thomas Donohue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface GameViewController : UIViewController

- (void) setDimensions: (CGSize) dimensions;

- (void) setTimedMode: (BOOL) timed;


@end
