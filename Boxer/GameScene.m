//
//  GameScene.m
//  Boxer
//
//  Created by Thomas Donohue on 4/21/16.
//  Copyright (c) 2016 Thomas Donohue. All rights reserved.
//


#import "GameScene.h"
#import "BoxerTypes.h"
#import "BoxerNode.h"
#import "BoxerGrid.h"
#import "BoxerConstants.h"
#import "ControlPad.h"

#import "AMSmoothAlert/AMSmoothAlertView.h"
#import "LTMorphingLabel/LTMorphingLabel-Swift.h"
#import "MZTimerLabel/MZTimerLabel.h"

@implementation GameScene {
    CGSize dimensions;
    ControlPad *arrowControls;
    
    BoxerNode *initialNode;
    
    BoxerNode *currentNode;
    
    BOOL timedMode;
    
    NSInteger score;
    LTMorphingLabel *scoreLabel;
    UILabel *countdownLabel;
    MZTimerLabel *timer;
    
}

- (void) setDimensions: (CGSize) dims
{
    dimensions = dims;
}

- (void) setTimedMode: (BOOL) timed
{
    timedMode = timed;
}

-(void)didMoveToView:(SKView *)view {
    [self beginGame];
}

- (void) beginGame
{
    UIFont *font = [UIFont fontWithName:@"Helvetica" size: 24];
    
    CGRect scoreFrame = CGRectMake(self.size.width / 32, 2 * self.size.height / 32, 125, 50);
    scoreLabel = [[LTMorphingLabel alloc] initWithFrame: scoreFrame];
    [self updateScore];
    [scoreLabel setMorphingEffect: 1];
    [scoreLabel setFont: font];
    [self.view addSubview: scoreLabel];
    
    CGRect countdownFrame = CGRectMake(self.size.width / 2, 2 * self.size.height / 32, 125, 50);
    countdownLabel = [[UILabel alloc] initWithFrame: countdownFrame];
    [countdownLabel setFont: font];
    if(timedMode) {
        [self.view addSubview: countdownLabel];
    }
    timer = [[MZTimerLabel alloc] initWithLabel: countdownLabel andTimerType: MZTimerLabelTypeTimer];
    timer.delegate = self;
    [timer setCountDownTime: COUNTDOWN_TIME];
    [timer start];
    
    CGFloat gridSize = 13 * self.size.width / 32;
    
    CGRect gameFrame = CGRectMake(19 * self.size.width / 32, self.size.height / 3, gridSize, gridSize);
    
    self.grid = [[BoxerGrid alloc] initGridWithFrame: gameFrame
                                       andDimensions: dimensions];
    [self.grid createConnections];
    [self addChild: self.grid];
    
    initialNode = NULL;
    currentNode = NULL;
    
    UIPanGestureRecognizer *gestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanSwipe:)];
    [[self view] addGestureRecognizer: gestureRecognizer];
    
    arrowControls = [[ControlPad alloc] initWithOrigin: CGPointMake(self.size.width/2, self.size.height / 5)
                                            andSpacing: SPACING * floor(((self.size.width/ 10)/SPACING)+0.5)];
    // [self addChild: arrowControls];
    

    
}

- (void) endGame
{
    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]
                                initDropAlertWithTitle: [NSString stringWithFormat:@"Score: %d", score]
                                andText: @"Play Again?"
                                andCancelButton: YES
                                forAlertType: AlertInfo];
    SKAction* waitAction = [SKAction waitForDuration: 1.0];
    SKAction* showAction = [SKAction runBlock:^{
        [alert show];
    }];
    [self runAction:[SKAction sequence:@[waitAction, showAction]]];
    alert.completionBlock = ^void (AMSmoothAlertView *alertObj, UIButton *button) {
        if(button == alertObj.defaultButton) {
            NSArray *subviews = [self.view subviews];
            for(UIView *v in subviews) {
                [v removeFromSuperview];
            }
            [self removeAllChildren];
            [self removeAllActions];
            [self beginGame];
        }
    };
}
- (void) timerLabel:(MZTimerLabel *)timerLabel finshedCountDownTimerWithTime:(NSTimeInterval)countTime {
    if(timedMode) {
        [self endGame];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // No multi touch
    if ([touches count] > 1) {
        return;
    }
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode: self];
        // NSLog(@"Touch Location: x- %f  y- %f", location.x, location.y);
        SKNode* nodeAtPoint = [self nodeAtPoint: location];
        if(CGRectContainsPoint(self.grid.frame, location) || [nodeAtPoint.name isEqual: @"node"]) {
            BoxerNode *nearestNode = [self.grid findNearestNode: location];
            // NSLog(@"Nearest Node at (%ld, %ld)", (long)nearestNode.location.x, (long)nearestNode.location.y );
            if(initialNode == NULL && currentNode == NULL) {
                // first move
                initialNode = nearestNode;
                currentNode = initialNode;
                [nearestNode drawNode: [SKColor redColor]];
            } else {
                [self moveTo: nearestNode];
            }
        }else if ([[nodeAtPoint.name substringFromIndex: [nodeAtPoint.name length] - 4] isEqual: @"Node"]) {
             // [arrowControls highlightArrow: nodeAtPoint.name];
        }
    }
    
}
- (void) updateScore
{
    score = self.grid.numFilledBoxes;
    [scoreLabel setText: [NSString stringWithFormat:@"Score: %d", score]];
    
}

- (void) moveTo: (BoxerNode*) destNode
{
    Path *path;
    
    path = [self.grid getPathFrom:currentNode To: destNode];
    if(path != NULL) {
        if(path.state == PATH_FREE){
            [path fillPath];
            [self.grid drawLineFrom: currentNode To: destNode];
            [currentNode drawNode: [SKColor blackColor]];
            currentNode = destNode;
            [destNode drawNode: [SKColor redColor]];
            [self.grid updateBoxes];
            [self updateScore];
            
            if(![self.grid possibleMovesFrom: currentNode]) {
                [self endGame];
            } else {
                [timer reset];
            }
        }
    }
}

- (void)handlePanSwipe:(UIPanGestureRecognizer*)recognizer
{
    // detect the swipe gesture
    if (recognizer.state == UIGestureRecognizerStateEnded && currentNode != NULL)
    {
        BoxerLocation currentLoc = {.x = currentNode.location.x, .y = currentNode.location.y};
        
        BoxerLocation topLoc = {.x = currentNode.location.x, .y = currentNode.location.y + 1};
        BoxerLocation topRightLoc = {.x = currentNode.location.x + 1, .y = currentNode.location.y + 1};
        BoxerLocation rightLoc = {.x = currentNode.location.x + 1, .y = currentNode.location.y};
        BoxerLocation rightBotLoc = {.x = currentNode.location.x + 1, .y = currentNode.location.y - 1};
        BoxerLocation botLoc = {.x = currentNode.location.x, .y = currentNode.location.y - 1};
        BoxerLocation botLeftLoc = {.x = currentNode.location.x - 1, .y = currentNode.location.y - 1};
        BoxerLocation leftLoc = {.x = currentNode.location.x - 1, .y = currentNode.location.y};
        BoxerLocation topLeftLoc = {.x = currentNode.location.x - 1, .y = currentNode.location.y + 1};
        
        //------------
        
        BoxerNode *topNode;
        BoxerNode *topRightNode;
        BoxerNode *rightNode;
        BoxerNode *rightBotNode;
        BoxerNode *botNode;
        BoxerNode *botLeftNode;
        BoxerNode *leftNode;
        BoxerNode *topLeftNode;
        
        CGPoint vel = [recognizer velocityInView:recognizer.view];
        
        if (vel.x < SWIPE_LEFT_THRESHOLD)
        {
            if (vel.y < SWIPE_UP_THRESHOLD)
            {
                // move left up
                [arrowControls highlightArrow: @"leftUpArrowNode"];
                
                if(currentLoc.x > 0 && currentLoc.y < self.grid.dimensions.height) {
                    topLeftNode = [self.grid getNodeInArrayAtLocation: topLeftLoc];
                    [self moveTo: topLeftNode];
                }
            }
            else if (vel.y > SWIPE_DOWN_THRESHOLD)
            {
                // move left down
                [arrowControls highlightArrow: @"leftDownArrowNode"];
                if(currentLoc.x > 0 && currentLoc.y > 0) {
                    botLeftNode = [self.grid getNodeInArrayAtLocation: botLeftLoc];
                    [self moveTo: botLeftNode];
                }
            }else {
                // move left
                [arrowControls highlightArrow: @"leftArrowNode"];
                if(currentLoc.x > 0) {
                    leftNode = [self.grid getNodeInArrayAtLocation: leftLoc];
                    [self moveTo: leftNode];
                }
            }
        }
        else if (vel.x > SWIPE_RIGHT_THRESHOLD)
        {
            if (vel.y < SWIPE_UP_THRESHOLD)
            {
                // move right up
                [arrowControls highlightArrow: @"rightUpArrowNode"];
                if(currentLoc.x < self.grid.dimensions.width && currentLoc.y < self.grid.dimensions.height) {
                    topRightNode = [self.grid getNodeInArrayAtLocation: topRightLoc];
                    [self moveTo: topRightNode];
                }
            }
            else if (vel.y > SWIPE_DOWN_THRESHOLD)
            {
                // move right down
                [arrowControls highlightArrow: @"rightDownArrowNode"];
                if(currentLoc.x < self.grid.dimensions.width && currentLoc.y > 0) {
                    rightBotNode = [self.grid getNodeInArrayAtLocation: rightBotLoc];
                    [self moveTo: rightBotNode];
                }
            }else{
                // move right
                [arrowControls highlightArrow: @"rightArrowNode"];
                if(currentLoc.x < self.grid.dimensions.width) {
                    rightNode = [self.grid getNodeInArrayAtLocation: rightLoc];
                    [self moveTo: rightNode];
                }
                
            }
        }
        else if (vel.y < SWIPE_UP_THRESHOLD)
        {
            // move up
            [arrowControls highlightArrow: @"upArrowNode"];
            if(currentLoc.y < self.grid.dimensions.height) {
                topNode = [self.grid getNodeInArrayAtLocation: topLoc];
                [self moveTo: topNode];
            }
        }
        else if (vel.y > SWIPE_DOWN_THRESHOLD)
        {
            // move down
            [arrowControls highlightArrow: @"downArrowNode"];
            if(currentLoc.y > 0) {
                botNode = [self.grid getNodeInArrayAtLocation: botLoc];
                [self moveTo: botNode];
            }
        }
        else
        {
            // the user lifted the finger/fingers but didn't swipe.
        }
    }
}

@end
