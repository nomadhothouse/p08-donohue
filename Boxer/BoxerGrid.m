//
//  BoxerGrid.m
//  Boxer
//
//  Created by Thomas Donohue on 4/21/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import "BoxerGrid.h"
#import "BoxerNode.h"
#import "BoxerTypes.h"
#import "BoxerConstants.h"
#import "BoxerUtils.h"
#import "Path.h"

@implementation BoxerGrid

- (id) initGridWithFrame: (CGRect) gridFrame andDimensions: (CGSize) dimensions
{
    if((self = [super init])) {
        
        CGFloat rowHeight = gridFrame.size.height / dimensions.height;
        CGFloat columnWidth = gridFrame.size.width / dimensions.width;
        // NSLog(@"columnWidth: %f: ", columnWidth);
        
        self.strokeColor = [SKColor blackColor];
        self.lineWidth = 2;
        
        CGMutablePathRef pathToDraw = CGPathCreateMutable();
        
        self.nodeArr = [[NSMutableArray alloc] init];
        self.pathArr = [[NSMutableArray alloc] init];
        self.boxArr = [[NSMutableArray alloc] init];
        
        _numFilledBoxes = 0;
        
        for (NSInteger i = 0; i <= dimensions.height; i++) {
            for (NSInteger j = 0; j <= dimensions.width; j++) {
                BoxerLocation nodeLocation = {.x = j, .y = i};

                BoxerNode *node = [[BoxerNode alloc] initNodeWithSize: floor(gridFrame.size.width / NODE_SCALE)
                                                          andLocation: nodeLocation];
                
                [node setPosition:CGPointMake(gridFrame.origin.x / 2 + j * columnWidth, gridFrame.origin.y / 2  + i * rowHeight)];
                node.zPosition = 5;
                [self addChild: node];
                [self.nodeArr addObject: node];
                
                // vertical lines
                CGPathMoveToPoint(pathToDraw, NULL, gridFrame.origin.x / 2 + j * columnWidth, gridFrame.origin.y / 2);
                CGPathAddLineToPoint(pathToDraw, NULL, gridFrame.origin.x / 2 + j * columnWidth,  gridFrame.origin.y / 2 +  dimensions.height * rowHeight);
                
                // diagonal lines - drawing them on greater than 8x8 crashes due to OpenGL glitch
                if(i < dimensions.height && dimensions.height < 8 && dimensions.width < 8) {
                    if(j > 0) {
                        CGPathMoveToPoint(pathToDraw, NULL, gridFrame.origin.x / 2 + j * columnWidth, gridFrame.origin.y / 2  + i * rowHeight);
                        CGPathAddLineToPoint(pathToDraw, NULL, gridFrame.origin.x / 2 + (j - 1) * columnWidth, gridFrame.origin.y / 2  + (i + 1) * rowHeight);
                    }
                    if(j < dimensions.width) {
                        CGPathMoveToPoint(pathToDraw, NULL, gridFrame.origin.x / 2 + j * columnWidth, gridFrame.origin.y / 2  + i * rowHeight);
                        CGPathAddLineToPoint(pathToDraw, NULL, gridFrame.origin.x / 2 + (j + 1) * columnWidth, gridFrame.origin.y / 2  + (i + 1) * rowHeight);
                    }
                }
            }
            // horizontal lines
            CGPathMoveToPoint(pathToDraw, NULL, gridFrame.origin.x / 2, gridFrame.origin.y / 2  + i * rowHeight);
            CGPathAddLineToPoint(pathToDraw, NULL, gridFrame.origin.x / 2 +  dimensions.width * columnWidth,  gridFrame.origin.y / 2  + i * rowHeight);
        }
        
        self.path = pathToDraw;
        
        self.gridFrame = gridFrame;
        self.dimensions = dimensions;
        self.rowHeight = rowHeight;
        self.columnWidth = columnWidth;
        

    }
    return self;
    
}

- (BoxerNode*) findNearestNode: (CGPoint) touchPoint {
    BoxerNode *nearestNode = NULL;
    CGFloat minDistance = -1;
    for(BoxerNode *node in self.nodeArr){
        CGFloat distance = CGDistance(touchPoint, node.position);
        if(minDistance < 0|| distance < minDistance) {
            minDistance = distance;
            nearestNode = node;
        }
    }
    return nearestNode;
}

- (void) createConnections {

    // Create Paths
    for(BoxerNode *node in self.nodeArr){
        BoxerLocation topLoc = {.x = node.location.x, .y = node.location.y + 1};
        BoxerLocation topRightLoc = {.x = node.location.x + 1, .y = node.location.y + 1};
        BoxerLocation rightLoc = {.x = node.location.x + 1, .y = node.location.y};
        BoxerLocation topLeftLoc = {.x = node.location.x - 1, .y = node.location.y + 1};
        
        //------------
        
        BoxerNode *topNode;
        BoxerNode *topRightNode;
        BoxerNode *rightNode;
        BoxerNode *topLeftNode;
        
        if(node.location.y  < self.dimensions.height ) {
            topNode = [self getNodeInArrayAtLocation: topLoc];
            Path *connectTop = [[Path alloc] initWithNode1: node andNode2: topNode];
            [self.pathArr addObject: connectTop];
            
            if(node.location.x  > 0) {
                topLeftNode = [self getNodeInArrayAtLocation: topLeftLoc];
                Path *connectTopLeft = [[Path alloc] initWithNode1: node andNode2: topLeftNode];
                [self.pathArr addObject: connectTopLeft];
            }
            if(node.location.x  < self.dimensions.width) {
                topRightNode = [self getNodeInArrayAtLocation: topRightLoc];
                Path *connectTopRight = [[Path alloc] initWithNode1: node andNode2: topRightNode];
                [self.pathArr addObject: connectTopRight];
            }
        }
        if(node.location.x  < self.dimensions.width) {
            rightNode = [self getNodeInArrayAtLocation: rightLoc];
            Path *connectRight = [[Path alloc] initWithNode1: node andNode2: rightNode];
            [self.pathArr addObject: connectRight];
        }

    }
    // Create Boxes
    for(BoxerNode *node in self.nodeArr){
        BoxerLocation nodeLoc = {.x = node.location.x, .y = node.location.y};
        
        BoxerLocation topLoc = {.x = node.location.x, .y = node.location.y + 1};
        BoxerLocation topRightLoc = {.x = node.location.x + 1, .y = node.location.y + 1};
        BoxerLocation rightLoc = {.x = node.location.x + 1, .y = node.location.y};
        
        //------------
        
        BoxerNode *topNode;
        BoxerNode *topRightNode;
        BoxerNode *rightNode;
        
        if(node.location.y  < self.dimensions.height && node.location.x  < self.dimensions.width) {
            topNode = [self getNodeInArrayAtLocation: topLoc];
            topRightNode = [self getNodeInArrayAtLocation: topRightLoc];
            rightNode = [self getNodeInArrayAtLocation: rightLoc];
            
            //six paths that form a box
            Path *path1 = [self getPathFrom: node To: rightNode];
            Path *path2 = [self getPathFrom: node To: topNode];
            Path *path3 = [self getPathFrom: node To: topRightNode];
            
            Path *path4 = [self getPathFrom: topNode To: topRightNode];
            Path *path5 = [self getPathFrom: topRightNode To: rightNode];
            Path *path6 = [self getPathFrom: topNode To: rightNode];
            
            Box *box = [[Box alloc] initWithLocation: nodeLoc];
            [box.boxPaths addObject: path1];
            [box.boxPaths addObject: path2];
            [box.boxPaths addObject: path3];
            [box.boxPaths addObject: path4];
            [box.boxPaths addObject: path5];
            [box.boxPaths addObject: path6];
            [self.boxArr addObject: box];
            NSLog(@"Make box (%d, %d)" , nodeLoc.x, nodeLoc.y);
            
        }
    }
    /**for(Path *path in self.pathArr) {
         NSLog(@"Path from (%d, %d) to (%d, %d)", path.firstNode.location.x, path.firstNode.location.y, path.secondNode.location.x, path.secondNode.location.y);
    }
    NSLog(@"num paths: %d", [self.pathArr count]);**/
}

- (BoxerNode*) getNodeInArrayAtLocation: (BoxerLocation) location
{
    return [self.nodeArr objectAtIndex: ((NSInteger)self.dimensions.width + 1) * location.y + location.x];
    
}

- (Path*) getPathFrom: (BoxerNode*) node1 To: (BoxerNode*) node2
{
    for(Path *path in self.pathArr) {
        if( ( (path.firstNode.location.x == node1.location.x && path.firstNode.location.y == node1.location.y &&
               path.secondNode.location.x == node2.location.x && path.secondNode.location.y == node2.location.y) ||
             (path.secondNode.location.x == node1.location.x && path.secondNode.location.y == node1.location.y &&
              path.firstNode.location.x == node2.location.x && path.firstNode.location.y == node2.location.y) ))
        {
            return path;
        }
    }
    return NULL;
}

- (void) drawLineFrom: (BoxerNode*) node1 To: (BoxerNode*) node2
{

    SKShapeNode* line = [SKShapeNode node];
    line.strokeColor = [SKColor redColor];
    line.lineWidth = 4;
    CGMutablePathRef pathToDraw = CGPathCreateMutable();
    CGPathMoveToPoint(pathToDraw, NULL, node1.position.x, node1.position.y);
    CGPathAddLineToPoint(pathToDraw, NULL, node2.position.x,  node2.position.y);
    line.path = pathToDraw;
    // line.zPosition = 6;
    [self addChild: line];
    
}

- (void) fillClearedBox: (BoxerLocation) boxLocation
{
    SKShapeNode* box = [SKShapeNode node];
    SKColor* fillColor = [SKColor colorWithRed:(217.0f/255.0f) green:(30.0f/255.0f) blue: (24.0f/255.0f) alpha: 1.0];
    box.strokeColor = [SKColor redColor];
    CGMutablePathRef pathToDraw = CGPathCreateMutable();
    BoxerNode *refNode = [self getNodeInArrayAtLocation: boxLocation];
    CGRect rect = CGRectMake(refNode.position.x, refNode.position.y, _columnWidth, _rowHeight);
    CGPathAddRect(pathToDraw, NULL, rect);
    box.path = pathToDraw;
    [box setFillColor: fillColor];
    box.zPosition = 4;
    [self addChild: box];
    
}

- (BOOL) possibleMovesFrom: (BoxerNode*) node
{
    for(Path *path in self.pathArr) {
        if(((path.firstNode.location.x == node.location.x && path.firstNode.location.y == node.location.y) ||
            (path.secondNode.location.x == node.location.x && path.secondNode.location.y == node.location.y)) && path.state == PATH_FREE)
        {
            return YES;
        }
    }
    return NO;
}

- (void) updateBoxes
{
    int count = 0;
    for(Box *box in self.boxArr) {
        [box checkBox];
        if(box.state == BOX_CLOSED) {
            [self fillClearedBox: box.location];
            count++;
        }
    }
    _numFilledBoxes = count;
}

@end
