//
//  Box.h
//  Boxer
//
//  Created by Thomas Donohue on 4/24/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Path.h"

@interface Box : NSObject

- (id) initWithLocation: (BoxerLocation) location;
- (void) checkBox;

@property (nonatomic) BoxerLocation location;

@property (nonatomic) enum BoxStateType state;

@property(strong, nonatomic) NSMutableArray<Path* >* boxPaths;

@end
