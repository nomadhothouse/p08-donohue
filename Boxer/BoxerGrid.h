//
//  BoxerGrid.h
//  Boxer
//
//  Created by Thomas Donohue on 4/21/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Box.h"
#import "BoxerNode.h"
#import "Path.h"

@interface BoxerGrid : SKShapeNode

@property(strong, nonatomic)  NSMutableArray<BoxerNode* >* nodeArr;

@property(strong, nonatomic) NSMutableArray<Path* >* pathArr;

@property(strong, nonatomic)  NSMutableArray<Box* >* boxArr;

@property (nonatomic) CGFloat rowHeight;
@property (nonatomic) CGFloat columnWidth;
@property (nonatomic) CGRect gridFrame;
@property (nonatomic) CGSize dimensions;

@property (nonatomic) NSInteger numFilledBoxes;

- (id) initGridWithFrame: (CGRect) gridFrame andDimensions: (CGSize) dimensions;
- (BoxerNode*) findNearestNode: (CGPoint) touchPoint;
- (void) createConnections;

- (BoxerNode*) getNodeInArrayAtLocation: (BoxerLocation) location;

- (Path*) getPathFrom: (BoxerNode*) node1 To: (BoxerNode*) node2;
- (void) drawLineFrom: (BoxerNode*) node1 To: (BoxerNode*) node2;
- (void) fillClearedBox: (BoxerLocation) boxLocation;

- (BOOL) possibleMovesFrom: (BoxerNode*) node;
- (void) updateBoxes;

@end
