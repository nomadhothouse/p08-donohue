//
//  ArrowNode.m
//  Boxer
//
//  Created by Thomas Donohue on 4/24/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import "ArrowNode.h"

@implementation ArrowNode

- (id) initNodeWithWidth: (CGFloat) aWidth
{
    if((self = [super init])) {
        self.aWidth = aWidth;
        
        self.strokeColor = [SKColor blackColor];
        
        CGMutablePathRef pathToDraw = CGPathCreateMutable();
        self.path = pathToDraw;
    }
    return self;
}


@end
