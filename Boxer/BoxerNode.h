//
//  BoxerNode.h
//  Boxer
//
//  Created by Thomas Donohue on 4/21/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "BoxerTypes.h"
#import "Path.h"

@interface BoxerNode : SKShapeNode

- (id) initNodeWithSize: (CGFloat) nodeSize andLocation: (BoxerLocation) location;
- (void) drawNode: (SKColor*) color;

@property (nonatomic) CGSize size;
@property (nonatomic) BoxerLocation location;

@end
