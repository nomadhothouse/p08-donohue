//
//  BoxerTypes.h
//  Boxer
//
//  Created by Thomas Donohue on 4/21/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#ifndef BoxerTypes_h
#define BoxerTypes_h

typedef struct {
    NSInteger x;
    NSInteger y;
} BoxerLocation;

typedef enum PathStateType
{
    PATH_FREE,
    PATH_FILLED
} PathState;

typedef enum BoxStateType
{
    BOX_OPEN,
    BOX_CLOSED
} BoxState;

#endif /* BoxerTypes_h */
