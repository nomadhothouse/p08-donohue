//
//  ControlPad.h
//  Boxer
//
//  Created by Thomas Donohue on 4/24/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface ControlPad : SKShapeNode

- (id) initWithOrigin: (CGPoint) origin andSpacing: (CGFloat) spacing;

- (void) highlightArrow: (NSString*) name;

@end
