//
//  ViewController.m
//  Boxer
//
//  Created by Thomas Donohue on 4/17/16.
//  Copyright Â© 2016 Thomas Donohue. All rights reserved.
//

#import "MenuViewController.h"
#import "HTPressableButton.h"
#import "UIColor+HTColor.h"
#import "SevenSwitch/SevenSwitch-Swift.h"

@interface MenuViewController ()

@property (nullable) GameViewController *boxerGameController;

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSInteger buttonWidth = self.view.frame.size.width / 2;
    NSInteger buttonHeight = self.view.frame.size.width / 10;
    
    CGRect frame1 = CGRectMake((self.view.frame.size.width - buttonWidth) / 2, self.view.frame.size.height / 4, buttonWidth, buttonHeight );
    HTPressableButton *button1 = [[HTPressableButton alloc] initWithFrame: frame1 buttonStyle:HTPressableButtonStyleRect];
    button1 .buttonColor = [UIColor ht_grapeFruitColor];
    button1 .shadowColor = [UIColor ht_grapeFruitDarkColor];
    [button1  setTitle:@"4x4" forState:UIControlStateNormal];
    button1.tag = 0;
    [button1 addTarget: self action:@selector(startGame:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: button1];
    
    CGRect frame2 = CGRectMake((self.view.frame.size.width - buttonWidth) / 2, self.view.frame.size.height / 3, buttonWidth, buttonHeight );
    HTPressableButton *button2 = [[HTPressableButton alloc] initWithFrame: frame2 buttonStyle:HTPressableButtonStyleRect];
    button2 .buttonColor = [UIColor ht_grapeFruitColor];
    button2 .shadowColor = [UIColor ht_grapeFruitDarkColor];
    [button2  setTitle:@"5x5" forState:UIControlStateNormal];
    button2.tag = 1;
    [ button2 addTarget: self action:@selector(startGame:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:  button2];
    
    CGRect frame3 = CGRectMake((self.view.frame.size.width - buttonWidth)/ 2, 53 * self.view.frame.size.height / 128, buttonWidth, buttonHeight );
    HTPressableButton *button3 = [[HTPressableButton alloc] initWithFrame: frame3 buttonStyle:HTPressableButtonStyleRect];
    button3 .buttonColor = [UIColor ht_grapeFruitColor];
    button3 .shadowColor = [UIColor ht_grapeFruitDarkColor];
    [button3  setTitle:@"6x6" forState:UIControlStateNormal];
    button3.tag = 2;
    [ button3 addTarget: self action:@selector(startGame:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:  button3];
    
    CGRect frame4 = CGRectMake((self.view.frame.size.width - buttonWidth) / 2, self.view.frame.size.height / 2, buttonWidth, buttonHeight );
    HTPressableButton *button4 = [[HTPressableButton alloc] initWithFrame: frame4 buttonStyle:HTPressableButtonStyleRect];
    button4 .buttonColor = [UIColor ht_grapeFruitColor];
    button4 .shadowColor = [UIColor ht_grapeFruitDarkColor];
    [button4  setTitle:@"7x7" forState:UIControlStateNormal];
    button4.tag = 3;
    [ button4 addTarget: self action:@selector(startGame:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:  button4];
    
    CGRect labelFrame = CGRectMake(self.view.frame.size.width / 4, 3 * self.view.frame.size.height / 4, buttonWidth, buttonHeight / 2 );
    UILabel *countdownLabel = [[UILabel alloc] init];
    countdownLabel.frame = labelFrame;
    UIFont *font = [UIFont fontWithName:@"Helvetica" size: 24];
    countdownLabel.text = @"Timed mode: ";
    countdownLabel.font = font;
    [self.view addSubview: countdownLabel];
    
    CGRect countdownFrame = CGRectMake(35 * self.view.frame.size.width / 64, 3 * self.view.frame.size.height / 4, buttonWidth / 4, buttonHeight / 2 );
    _countdownSwitch = [[SevenSwitch alloc] init];
    [_countdownSwitch addTarget: self action:@selector(switchPressed:) forControlEvents: UIControlEventValueChanged];
    [_countdownSwitch setIsRounded: NO];
    _countdownSwitch.frame = countdownFrame;
    [self.view addSubview: _countdownSwitch];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)switchPressed:(id)sender {
    [self.boxerGameController setTimedMode: _countdownSwitch.isOn];
}

- (IBAction)startGame:(id)sender {
    CGSize dimensions = CGSizeMake(5, 5);
    // NSLog(@"%d", [sender tag]);
    switch([sender tag]) {
        case 0:
            dimensions = CGSizeMake(4, 4);
            break;
        case 1:
            dimensions = CGSizeMake(5, 5);
            break;
        case 2:
            dimensions = CGSizeMake(6, 6);
            break;
        case 3:
            dimensions = CGSizeMake(7, 7);
            break;
            
    }
    self.boxerGameController = [self.storyboard instantiateViewControllerWithIdentifier:@"GameController"];
    [self.boxerGameController setDimensions: dimensions];
    [self.boxerGameController setTimedMode: _countdownSwitch.isOn];
    
    
    [self.navigationController pushViewController: self.boxerGameController animated:YES];
}

@end