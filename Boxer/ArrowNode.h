//
//  ArrowNode.h
//  Boxer
//
//  Created by Thomas Donohue on 4/24/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface ArrowNode : SKShapeNode

- (id) initNodeWithWidth: (CGFloat) aWidth;

@property (nonatomic) CGFloat aWidth;

@end
