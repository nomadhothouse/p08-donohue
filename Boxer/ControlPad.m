//
//  ControlPad.m
//  Boxer
//
//  Created by Thomas Donohue on 4/24/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#include <math.h>

#import "ControlPad.h"

@implementation ControlPad

- (id) initWithOrigin: (CGPoint) origin andSpacing: (CGFloat) spacing;
{
    if((self = [super init])) {
        SKSpriteNode *leftArrowNode = [SKSpriteNode spriteNodeWithImageNamed:@"b-arrow.png"];
        leftArrowNode.position = CGPointMake(origin.x - spacing, origin.y);
        leftArrowNode.name = @"leftArrowNode";
        [self addChild: leftArrowNode];
        
        SKSpriteNode *rightArrowNode = [SKSpriteNode spriteNodeWithImageNamed:@"b-arrow.png"];
        rightArrowNode.position = CGPointMake(origin.x + spacing, origin.y);
        rightArrowNode.name = @"rightArrowNode";
        rightArrowNode.zRotation = M_PI;
        [self addChild: rightArrowNode];
        
        
        SKSpriteNode *upArrowNode = [SKSpriteNode spriteNodeWithImageNamed:@"b-arrow.png"];
        upArrowNode.position = CGPointMake(origin.x, origin.y + spacing);
        upArrowNode.name = @"upArrowNode";
        upArrowNode.zRotation = -M_PI_2;
        [self addChild: upArrowNode];
        
        SKSpriteNode *downArrowNode = [SKSpriteNode spriteNodeWithImageNamed:@"b-arrow.png"];
        downArrowNode.position = CGPointMake(origin.x, origin.y - spacing);
        downArrowNode.name = @"downArrowNode";
        downArrowNode.zRotation = M_PI_2;
        [self addChild: downArrowNode];
        
        
        SKSpriteNode *leftUpArrowNode = [SKSpriteNode spriteNodeWithImageNamed:@"b-arrow.png"];
        leftUpArrowNode.position = CGPointMake(origin.x - 3 * spacing / 4, origin.y + 3 * spacing / 4);
        leftUpArrowNode.name = @"leftUpArrowNode";
        leftUpArrowNode.zRotation = -M_PI_4;
        [self addChild: leftUpArrowNode];
        
        SKSpriteNode *rightUpArrowNode = [SKSpriteNode spriteNodeWithImageNamed:@"b-arrow.png"];
        rightUpArrowNode.position = CGPointMake(origin.x + 3 * spacing / 4, origin.y + 3 * spacing / 4);
        rightUpArrowNode.name = @"rightUpArrowNode";
        rightUpArrowNode.zRotation = -3 * M_PI_4;
        [self addChild: rightUpArrowNode];
        
        SKSpriteNode *leftDownArrowNode = [SKSpriteNode spriteNodeWithImageNamed:@"b-arrow.png"];
        leftDownArrowNode.position = CGPointMake(origin.x - 3 * spacing / 4, origin.y - 3 * spacing / 4);
        leftDownArrowNode.name = @"leftDownArrowNode";
        leftDownArrowNode.zRotation = M_PI_4;
        [self addChild: leftDownArrowNode];
        
        SKSpriteNode *rightDownArrowNode = [SKSpriteNode spriteNodeWithImageNamed:@"b-arrow.png"];
        rightDownArrowNode.position = CGPointMake(origin.x + 3 * spacing / 4, origin.y - 3 * spacing / 4);
        rightDownArrowNode.name = @"rightDownArrowNode";
        rightDownArrowNode.zRotation = 3 * M_PI_4;
        [self addChild: rightDownArrowNode];
        
    }
    return self;
    
}

- (void) highlightArrow: (NSString*) name
{
    for (SKSpriteNode *node in self.children) {
        if ([node.name isEqualToString: name]){
            SKAction* highlightAction1 = [SKAction runBlock:^{
                node.texture = [SKTexture textureWithImageNamed:@"r-arrow.png"];
            }];
            SKAction* waitAction = [SKAction waitForDuration: 0.4];
            SKAction* highlightAction2 = [SKAction runBlock:^{
                node.texture = [SKTexture textureWithImageNamed:@"b-arrow.png"];
            }];
            [node runAction:[SKAction sequence:@[highlightAction1, waitAction, highlightAction2]]];
            
        }
    }
    
}


@end
