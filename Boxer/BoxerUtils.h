//
//  BoxerUtils.h
//  Boxer
//
//  Created by Thomas Donohue on 4/23/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#ifndef BoxerUtils_h
#define BoxerUtils_h

#include <math.h>

CG_INLINE CGFloat CGDistance(CGPoint pt1, CGPoint pt2);

CG_INLINE CGFloat
CGDistance(CGPoint pt1, CGPoint pt2)
{
    double dx = (pt2.x - pt1.x);
    double dy = (pt2.y - pt1.y);
    return dx * dx + dy * dy;
}


#endif /* BoxerUtils_h */
