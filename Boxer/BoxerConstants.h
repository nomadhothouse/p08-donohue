//
//  BoxerConstants.h
//  Boxer
//
//  Created by Thomas Donohue on 4/21/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#ifndef BoxerConstants_h
#define BoxerConstants_h

static const int NODE_SCALE = 20;

static const CGFloat SWIPE_SPEED_THRESHOLD = 750;

static const CGFloat SWIPE_UP_THRESHOLD = -SWIPE_SPEED_THRESHOLD;
static const CGFloat SWIPE_DOWN_THRESHOLD = SWIPE_SPEED_THRESHOLD;
static const CGFloat SWIPE_LEFT_THRESHOLD = -SWIPE_SPEED_THRESHOLD;
static const CGFloat SWIPE_RIGHT_THRESHOLD = SWIPE_SPEED_THRESHOLD;

static const CGFloat SPACING = 100.0;

static const int COUNTDOWN_TIME = 4;

#endif /* BoxerConstants_h */
