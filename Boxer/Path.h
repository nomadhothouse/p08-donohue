//
//  Path.h
//  Boxer
//
//  Created by Thomas Donohue on 4/25/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BoxerTypes.h"
@class BoxerNode;

@interface Path : NSObject

- (id) initWithNode1: (BoxerNode*) node1 andNode2: (BoxerNode*) node2;

- (void) fillPath;

@property (nonatomic) BoxerNode* firstNode;
@property (nonatomic) BoxerNode* secondNode;

@property (nonatomic) enum PathStateType state;

@end
