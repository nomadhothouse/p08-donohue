//
//  MenuViewController.h
//  Boxer
//
//  Created by Thomas Donohue on 4/17/16.
//  Copyright Â© 2016 Thomas Donohue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameViewController.h"
#import "SevenSwitch/SevenSwitch-Swift.h"

@interface MenuViewController : UIViewController

@property (readonly, nullable) GameViewController *boxerGameController;
@property (readonly, nullable) SevenSwitch *countdownSwitch;
- (IBAction) startGame: (id)sender;

@end
