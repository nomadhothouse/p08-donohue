//
//  BoxerNode.m
//  Boxer
//
//  Created by Thomas Donohue on 4/21/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import "BoxerNode.h"
#import "BoxerTypes.h"

@implementation BoxerNode

- (id) initNodeWithSize: (CGFloat) nodeSize andLocation: (BoxerLocation) location
{
    if((self = [super init])) {
        self.name = @"node";
        self.size = CGSizeMake(nodeSize, nodeSize);
        self.location = location;
        
        [self drawNode: [SKColor blackColor]];
        
    }
    return self;
    
}

- (void) drawNode: (SKColor*) color
{
    [self setFillColor: color];
    
    CGMutablePathRef circlePath = CGPathCreateMutable();
    CGPathAddEllipseInRect(circlePath , NULL , CGRectMake(-self.size.width/2, -self.size.height/2, self.size.width, self.size.height) );
    self.path = circlePath;
}




@end
