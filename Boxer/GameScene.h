//
//  GameScene.h
//  Boxer
//

//  Copyright (c) 2016 Thomas Donohue. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "BoxerGrid.h"
#import "MZTimerLabel/MZTimerLabel.h"

@interface GameScene : SKScene<MZTimerLabelDelegate>

- (void) setDimensions: (CGSize) dimensions;
- (void) setTimedMode: (BOOL) timed;

@property (nonatomic) BoxerGrid* grid;

@end
