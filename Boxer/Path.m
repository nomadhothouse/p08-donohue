//
//  Path.m
//  Boxer
//
//  Created by Thomas Donohue on 4/25/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import "Path.h"

@implementation Path

- (id) initWithNode1: (BoxerNode*) node1 andNode2: (BoxerNode*) node2
{
    if((self = [super init])) {
        self.firstNode = node1;
        self.secondNode = node2;
        
        self.state = PATH_FREE;
    }
    return self;
    
}

- (void) fillPath
{
    self.state = PATH_FILLED;
}

@end
