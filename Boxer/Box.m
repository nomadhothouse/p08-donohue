//
//  Box.m
//  Boxer
//
//  Created by Thomas Donohue on 4/24/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import "Box.h"
#import "BoxerTypes.h"

@implementation Box

- (id) initWithLocation: (BoxerLocation) location
{
    if((self = [super init])) {
        self.location = location;
        self.state = BOX_OPEN;
        self.boxPaths = [[NSMutableArray alloc] init];
    }
    return self;
    
}

- (void) checkBox
{
    for(Path *path in self.boxPaths){
        if(path.state == PATH_FREE) {
            return;
        }
    }
    self.state = BOX_CLOSED;
    
}
@end
